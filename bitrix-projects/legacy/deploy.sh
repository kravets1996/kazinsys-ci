#!/bin/bash

if [ -n "$(git status --porcelain)" ]; then
  echo "Cannot deploy - changes on production"
  git status --porcelain

  # Пушим изменения в ветку production
  git branch -d production
  git add .
  git commit -m 'Production changes'
  git checkout -b production
  git push origin production
  git checkout master
  git branch -D production
  echo "Pushed changes to 'production' branch"

  exit 1
else
  echo "no changes"
  git pull origin master
  exit 0
fi
