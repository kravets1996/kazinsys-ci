#!/bin/bash

curl -LO https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/bitrix-projects/legacy/deploy.sh
curl -LO https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/bitrix-projects/create-merge-request.sh

chmod +x deploy.sh
chmod +x create-merge-request.sh

CMD="cd ${PRODUCTION_ROOTPATH} && bash -s"

ssh -p ${PRODUCTION_SSH_PORT} ${PRODUCTION_USER}@${PRODUCTION_HOST} "${CMD}" < ./deploy.sh

status=$?

[ $status -eq 1 ] && ./create-merge-request.sh $CI_PROJECT_ID $GITLAB_API_TOKEN

exit $status
