#!/bin/bash

PROJECT_ID=$1
ACCESS_TOKEN=$2

BODY="{
    \"id\": ${PROJECT_ID},
    \"source_branch\": \"production\",
    \"target_branch\": \"master\",
    \"remove_source_branch\": true,
    \"title\": \"Production Changes\"
}"

curl -X POST \
  --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
  --header "Content-Type: application/json" \
  --data "${BODY}" \
    https://$CI_SERVER_HOST/api/v4/projects/$PROJECT_ID/merge_requests
