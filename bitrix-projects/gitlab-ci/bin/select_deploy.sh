#!/bin/bash
secsSinceMidnight=$(( $(TZ='Asia/Almaty' date +%s) - $(TZ='Asia/Almaty' date -d '00:00:00' +%s) ))
weekDay=$(date +%u)
delay=0
dayDelay=86400

# Переносы до 11 утра следующего дня
if [[ $weekDay -lt 5 || ($weekDay == 5 && $secsSinceMidnight -lt 32400) ]]; then
  # От 00:00 до 9:00
  if [[ $secsSinceMidnight -lt 32400 ]]; then
    delay=$(((11 * 60 * 60) - secsSinceMidnight))
  fi

  # От 17:00 до 00:00
  if [[ $secsSinceMidnight -gt 61200 ]]; then
    # 126000 секунд - 11:00 утра + 1 сутки
    delay=$(((11 * 60 * 60) + dayDelay - secsSinceMidnight))
  fi
fi

# Переносы на выходных и в пятницу, после 14:00
if [[ $weekDay -gt 5 || ($weekDay == 5 && $secsSinceMidnight -gt 50400) ]]; then
  # Добавляем до 11 утра следующего дня
  delay=$((126000 - secsSinceMidnight))
  # Добавляем дни до понедельника
  delay=$((delay + ((7 - weekDay) * dayDelay)))
fi

if [[ $delay -gt 0 ]]; then
  # Delay
  curl -LO https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/bitrix-projects/gitlab-ci/jobs/delayed_deploy.yml
  export DEPLOY_DELAY=$((delay/60))
  envsubst < delayed_deploy.yml > deploy.yml
  echo Delay - $((delay/60)) minutes "($((delay/(60*60))) hours)"
  curl -X POST https://genie.kazinsys.kz/api/webhook/gitlab \
    -H Content-Type:application/json \
    -H Accept:application/json \
    -H X-Gitlab-Token:$X_GITLAB_TOKEN \
    -H 'X-Gitlab-Event: Delayed Deployment Hook' \
    -d "{\"project\":{\"id\":$CI_PROJECT_ID,\"name\":\"$CI_PROJECT_TITLE\",\"web_url\":\"$CI_PROJECT_URL\"},\"pipeline_url\":\"$CI_PIPELINE_URL\"}"
else
  curl -LO https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/bitrix-projects/gitlab-ci/jobs/deploy.yml
  echo Deploy
fi
