#!/bin/bash

set -e

TAG=$1

# Если изменения есть где-то кроме папки sites/
if [ -n "$(git status --porcelain . -- ':!sites')" ]; then
  # Ничего не делаем и выходим с ошибкой
  # Нужно заходить на сервер и проверять вручную, что там изменили
  echo "Changes outside of sites/ folder"
  echo "SSH to your server and commit changes manually"

  git status --porcelain

  exit 2
fi

git fetch

current_branch=$(git rev-parse --abbrev-ref HEAD)

# Проверяем наличие изменений в папке sites/ (ранее мы проверили изменения в любых других папках)
if [ -n "$(git status --porcelain)" ]; then
  # Пушим изменения в ветку production
  echo "There are changes"

  if [[ "$current_branch" != "production" ]]; then
    git branch -D production &>/dev/null || true
    git checkout -b production
  fi

  git add sites/
  git commit -m 'Production changes'

  git push origin production

  echo "Pushed changes to 'production' branch"
  git status --porcelain
  exit 1
elif [ -n "$(git cherry -v origin/master)" ]; then
  echo "There are not pushed local commits"

  if [[ "$current_branch" != "production" ]]; then
    git branch -D production &>/dev/null || true
    git checkout -b production
  fi

  git push origin production

  echo "Pushed commits to 'production' branch"
  exit 1
fi

if git ls-remote --exit-code --heads origin production; then
  if ! git merge-base --is-ancestor "origin/production" "origin/master"; then
      echo "Ошибка: Изменения из ветки production еще не включены в master. Завершите процесс обработки перед деплоем."
      exit 1
  fi
fi

echo "No changes found. Starting deploy."

if [ -n "$TAG" ]; then
    git checkout "$TAG"
else
    git pull origin master
fi

make composer-install npm-install npm-prod
rm -rf bitrix/cache
rm -rf bitrix/managed_cache

exit 0
