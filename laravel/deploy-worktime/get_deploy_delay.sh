#!/bin/bash
secsSinceMidnight=$(( $(TZ='Asia/Almaty' date +%s) - $(TZ='Asia/Almaty' date -d '00:00:00' +%s) ))
weekDay=$(date +%u)
delay=0
dayDelay=86400

# Переносы до 11 утра следующего дня
if [[ $weekDay -lt 5 || ($weekDay == 5 && $secsSinceMidnight -lt 32400) ]]; then
  # От 00:00 до 9:00
  if [[ $secsSinceMidnight -lt 32400 ]]; then
    delay=$(((11 * 60 * 60) - secsSinceMidnight))
  fi

  # От 17:00 до 00:00
  if [[ $secsSinceMidnight -gt 61200 ]]; then
    # 126000 секунд - 11:00 утра + 1 сутки
    delay=$(((11 * 60 * 60) + dayDelay - secsSinceMidnight))
  fi
fi

# Переносы на выходных и в пятницу, после 14:00
if [[ $weekDay -gt 5 || ($weekDay == 5 && $secsSinceMidnight -gt 50400) ]]; then
  # Добавляем до 11 утра следующего дня
  delay=$((126000 - secsSinceMidnight))
  # Добавляем дни до понедельника
  delay=$((delay + ((7 - weekDay) * dayDelay)))
fi

# Delay
echo $((delay/60))
